/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
 (function() {
    let JSPreviewer;
    let JSPreviewer_iframe;
    const ROOT_URL = "../jsruntime/test/";
    const PAGE_URL = "index.html";
    Vue.component('jspreviewer', {
        props: [],
        data() {
            return {
                enable: true
            };
        },
        template: '\
    <modal id="jspreviewer" @on-visible-change="onVisibleChange" :mask-closable="false" sticky :mask="false"\
     footer-hide draggable v-model="enable"  title="预览器" width="360"\
     closable="false" >\
    <iframe ref="jsprevieweriframe" id="jsprevieweriframe" scrolling="no"\
     src="' + ROOT_URL + PAGE_URL + '"\
     style="width:720px;height:1280px;border:0;zoom:0.5">\
    </frame>\
    </modal>',
        mounted() {
            JSPreviewer = this;
            JSPreviewer_iframe = this.$refs.jsprevieweriframe;
        },
        methods: {
            onVisibleChange(v) {
                if (!v) {
                    JSPreviewer = null;
                    JSPreviewer_iframe = null;
                    UB_IDE.removeSubwindowComponent('jspreviewer');
                }
            }
        }
    });
    Ublockly.onInited(() => {
        class JSConnector extends OBConnector {
            pageUrl;
            /**
             * @type {Window}
             */
            runEnv;
            constructor() {
                super();
                this.pageUrl = "../jsruntime/test/index.html";
            }
            loadConfig() {
                let env = ROOT_URL + 'env/'
                let jsarr = [
                    env + 'i18n_zh.js',
                    env + 'nativeEvent.js',
                    env + 'native.js',
                ];
                var xmlpath = env + "nativeBlocks.xml";
                Ublockly.loadNativeInfo(jsarr, xmlpath);
            }
            runProject() {
                Ublockly.exportExePackage((err, result) => {
                    if (!err) {
                        let runProjectCmd = { "cmd": "runProject", "bytes": result, fsm: "Start.Main" };
                        if (!JSPreviewer_iframe) {
                            UB_IDE.addSubwindowComponent('jspreviewer');
                            setTimeout(() => {
                                JSPreviewer_iframe.contentWindow.window.onload = () => {
                                    JSPreviewer_iframe.contentWindow.postMessage(runProjectCmd);
                                };
                            }, 0);
                        } else {
                            JSPreviewer_iframe.contentWindow.postMessage(runProjectCmd);
                        }
                    }
                });
            }
        }
        Ublockly.addConnector(new JSConnector());
    });
})();